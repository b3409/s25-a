const http = require('http');  
const host = 4000; 
const { users } = require('./data/users.js');
const {products} = require('./data/products.js');

let server = http.createServer((req, res) => {
    if (req.url === '/') {
	  //1st Task: Landing Page
	  res.writeHead(200, {'Content-Type':'text/plain'})
      res.end('Welcome to our B156 Online Store!');
	} else if (req.url === '/catalog' && req.method === 'GET') {
	  //we need to include an HTTP method type on each route to make sure that the server will only accept the correct type of request that describes the action that will done upon accessing the endpoint.

      //Display all the items available in our products directory. 
     	 res.writeHead(200, {'Content-Type':'application/json'});
      //to display each item as the response of this endpoint by using the write(), by calling out the component that describes our products resources.

      //we need to convert our resources into all STRINGS.
        //ue the JSON.stringify()
     	 res.write(JSON.stringify(products));
     	 res.end();  
	} else if (req.url === '/create' && req.method === 'POST') {
		      let reqBody = ''; 
      //identify what will be the anatomy of a product
        // (name, price, stock, color, category)
        //create a template in postman. 
      //start creating the streams fot us to able to emit events needed to create this new data. 
      req.on('data', (productInput) => {
          reqBody += productInput; 
          console.log(reqBody); 
      })
      //create a response 'end' step -> this will only run after the request has been completely sent. 
      req.on('end', () => {
          let usableData = JSON.parse(reqBody);
          console.log(usableData); 
          //console.log(typeof usableData)//object

          let newItem = {
             "itemName":  usableData.name,
             "itemPrice": usableData.price,
             "stocks": usableData.stocks,
             "itemColor": usableData.color,
             "category": usableData.category
          }

          //insert the object inside the collection
          //collection.push()
          products.push(newItem);
          res.writeHead(201, {'Content-Type': 'application/json'}); 
          res.write('A New Product Has Been Created!');
          res.end();
      })



	} else if (req.url === '/register' && req.method === 'POST') {
	
     //Execute a Create method in order to add more users in our users collection. 

       //1. Identify/describe the anatomy of a user
             //=> email, password, mobileNo, birthday, gender

       //2. Initialize/declare a 'requestBody'
             //What will be the purpose of 'reqBody'?
                //=> this will serve as a placeholder for the information/data that will created later on upon sending this post method in order to create a new resource
       let requestBody = ''; 

       //3. Create a 'stream' 
                //Stream => a sequence of data that are one of the fundamental concepts that powers Node.JS applications.
                //we need to emit an event that will happen upon accessing this endpoint in the established connection. 
                  //this 'on()' => is an example of an eventEmitter in Node JS. 
                  //EventEmitter -> is a module in NodeJS that facilitates communication/interaction between object in Node JS.
                  //the 'data' event will allow us to create a 'buffer'
                  //buffer => as a reserved segment/portion of a memory within a program that is used to hold data that is being processed
                  //create a method that will describe the procedure that will upon accessing this event. declare an argument that will catch the user's input. 
                  //'clientData' => input/received info from the client describing the new user to be created.
       req.on('data', (clientData) => {
          requestBody += clientData;
       }); 

       //4. Emit a new event called 'end' in the enpoint 
           //end event => will send/emit a signal that will determine if the received has already received a data. in this event describe what you want to do to the data that you receieved from the client
       req.on('end', () => {
          //create a checker to see the data you got from the client.
          //console.log(requestBody);
             //string (!readable or !useable)
          //parse the data from the client to make it more usable. 
          let convertedData = JSON.parse(requestBody); 
          console.log(convertedData); 
          console.log(typeof convertedData); 
          
          //save the information from the request and save it inside our users collection
             //1. get what information you would like to save.
          let newUser = {
            "emailAddress": convertedData.email,
            "userPassword": convertedData.password,
            "mobileNumber": convertedData.mobileNo,
            "gender": convertedData.gender,
            "birthday": convertedData.birthdate
          }
          
            //2. Insert the new data inside the collection

            //3. create the correct header
          users.push(newUser); 
          //http status => create /post
             //201 created
          res.writeHead(201, {'Content-Type': 'application/json'})
          res.write('Successfully created new account!');
          res.end(); //terminate the transmission of the stream.
       }); 
	} else if (req.url === '/admin' &&  req.method === 'GET') {
	  //4th Task
	  res.writeHead(200, {'Content-Type':'application/json'})
	  res.end(JSON.stringify(users)); 
	} else {
	  res.writeHead(404, {'Content-Type':'text/plain'}); 
      res.end('No page was Found'); 
	}

}) 
server.listen(host); 

console.log(`Listening on port: ${host}`); 

